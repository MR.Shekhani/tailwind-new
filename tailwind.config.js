/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      textColor:{
        primary: "#3E9FE3",
        yel:"#FFA900",
        four:"#107EC3",
        light:"#6F7CB2;"
      },
      fontSize:{
        50:"50px",
        40:"40px"
      },
      backgroundImage:{
        imgcircle:"url('/images/imgcircle.svg')",
      },
      width:{
        116:"116px",
        274:"274px"
      },
      backgroundColor:{
        sec: " #3E9FE3",
        ter: "rgba(62, 159, 227, 0.15);",
        pri: "#006CB8",
        yel:"rgba(255, 169, 0, 0.1)",
       linear:"(327.15deg, rgba(0, 117, 200, 0.8) 19.99%",
       linear2:"rgba(62, 159, 227, 0.8) 79.57%);"  
      },
      backgroundImage:{
        card:"url('/images/cardbg.svg');"
      },
      borderColor:{
        primary:"#3E9FE3"
      }
    },
  },
  plugins: [],
}
